require('dotenv').config()
'use strict';

const express = require('express')
const http = require('http')
const axios = require('axios').default;
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()
const httpServer = http.createServer(app);
console.log(process.env.PORT)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())
app.post("/api/cart/get", async (req, res) => {
    let { count } = req.body

    if (!count)
        return res.status(200).json({ error: "Items count is invalid" })

    try {
        let { data } = await axios.get(`https://5fd8a5357e05f000170d2d46.mockapi.io/api/cart?page=1&limit=${count}`)
        return res.status(200).json({ carts: data })
    } catch (e) {
        console.log("Err get cards", e)
        return res.status(200).json({ error: "Unexpected error get cards" })
    }
})
httpServer.listen(process.env.PORT, () => {
    console.log(`Express server available at http://127.0.0.1:${process.env.PORT}`);
});